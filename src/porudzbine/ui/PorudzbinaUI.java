package porudzbine.ui;

import java.time.LocalDateTime;
import java.util.Collection;

import porudzbine.DAO.PorudzbinaDAO;
import porudzbine.model.Porudzbina;
import porudzbine.model.Proizvod;
import porudzbine.util.Konzola;

public class PorudzbinaUI {

	private static PorudzbinaDAO porudzbinaDAO;
	
	public static void setPorudzbineDAO( PorudzbinaDAO porudzbinaDAO) {
		PorudzbinaUI.porudzbinaDAO = porudzbinaDAO;
	}

	
	public static void prikazSvih() {
		try {
			Collection<Porudzbina> porudzbina = porudzbinaDAO.getAll();

			System.out.println();
			for (Porudzbina itPorudzbina: porudzbina) {
				System.out.println(itPorudzbina);
				System.out.println(); 
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("Došlo je do greške!");
		}
	}
	
	public static void dodavanje() {
		try {
			Proizvod proizvod = ProizvodUI.pronalazenje();
			if (proizvod == null) {
				return;
			}
			String ulica = Konzola.ocitajString("Unesite ulicu");
			int broj = Konzola.ocitajInt("Unesite broj");

			LocalDateTime datumIVreme = LocalDateTime.now();
			Porudzbina porudzbina = new Porudzbina(0, datumIVreme, ulica, broj, proizvod);
			porudzbinaDAO.add(porudzbina);
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("Došlo je do greške!");
		}
	}
	
}
