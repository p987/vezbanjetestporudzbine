package porudzbine.ui;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import porudzbine.DAO.ProizvodDAO;
import porudzbine.model.Porudzbina;
import porudzbine.model.Proizvod;
import porudzbine.model.StavkaIzvestaja;
import porudzbine.util.Konzola;

public class IzvestavanjeUI {
	
	private static ProizvodDAO proizvodDAO;

	public static void setProizvodDAO(ProizvodDAO proizvodDAO) {
		IzvestavanjeUI.proizvodDAO = proizvodDAO;
	}

	public static void izvestavanje() {
	LocalDateTime pocetak = Konzola.ocitajDateTime("Unesite pocetak perioda");
	LocalDateTime kraj = Konzola.ocitajDateTime("Unesite kraj perioda");
//	LocalDateTime pocetak = LocalDateTime.parse("02.09.2021. 00:00", Konzola.getDateTimeFormatter());
//	LocalDateTime kraj =	LocalDateTime.parse("02.09.2021. 23:59", Konzola.getDateTimeFormatter());
	try {
		Collection<Proizvod> proizvodKolekcija = proizvodDAO.getAll();
		List<StavkaIzvestaja> stavkaIzvestajaList = new ArrayList<>();
		
		for (Proizvod itProizvod : proizvodKolekcija) {
			int brojPorudzbina = 0;
			LocalDateTime poslednjiDatum = LocalDateTime.parse("01.01.0001. 00:00", Konzola.getDateTimeFormatter());
			
			for (Porudzbina itPorudzbina : itProizvod.getPorudzbinaSet()) {
				if (itPorudzbina.isDatumIVremeUOpsegu(pocetak, kraj)) {
					brojPorudzbina++;
					if (poslednjiDatum.compareTo(itPorudzbina.getDatumIVreme()) < 0) {
						poslednjiDatum = itPorudzbina.getDatumIVreme();
					}
				}
			}
			double ukupanPrihod = brojPorudzbina * itProizvod.getCena();
			if (itProizvod.isBesplatnaDostava()) {
				ukupanPrihod += brojPorudzbina * 1000;
			}
			StavkaIzvestaja stavka = new StavkaIzvestaja(itProizvod.getSifra(), itProizvod.getNaziv(), ukupanPrihod, poslednjiDatum);
			stavkaIzvestajaList.add(stavka);
		}
									
		// sortiranje izveÅ¡taja
		stavkaIzvestajaList.sort(StavkaIzvestaja::compareBrojPoziva);

		// prikaz izveÅ¡taja
		System.out.println();
		for (StavkaIzvestaja itStavka: stavkaIzvestajaList) {
			System.out.println(itStavka);
		}
	} catch (Exception ex) {
		ex.printStackTrace();
		System.out.println("DoÅ¡lo je do greÅ¡ke!");
	}
	}
}
