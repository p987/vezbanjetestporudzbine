package porudzbine.ui;

import java.sql.Connection;
import java.sql.DriverManager;

import porudzbine.DAO.PorudzbinaDAO;
import porudzbine.DAO.ProizvodDAO;
import porudzbine.DAOImpl.PorudzbinaDAOImpl;
import porudzbine.DAOImpl.ProizvodDAOImpl;
import porudzbine.util.Meni;
import porudzbine.util.Meni.FunkcionalnaStavkaMenija;
import porudzbine.util.Meni.IzlaznaStavkaMenija;
import porudzbine.util.Meni.StavkaMenija;


public class Application {

	private static void initDatabase() throws Exception {
		Connection conn = DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/porudzbine?allowPublicKeyRetrieval=true&useSSL=false&serverTimezone=Europe/Belgrade", 
				"root", 
				"root");

		ProizvodDAO proizvodDAO = new ProizvodDAOImpl(conn);
		PorudzbinaDAO porudzbinaDAO = new PorudzbinaDAOImpl(conn);
		
		ProizvodUI.setProizvodDAO(proizvodDAO);
		PorudzbinaUI.setPorudzbineDAO(porudzbinaDAO);
		IzvestavanjeUI.setProizvodDAO(proizvodDAO);
	}
	
	static {
		try {
			initDatabase();
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("GreÅ¡ka pri povezivanju sa izvorom podataka!");
			
			System.exit(1); // prekid programa (u suprotnom bi se zapoÄ�ela main metoda)
		}
	}

	public static void main(String[] args) {
		Meni.pokreni("Porudzbine", new StavkaMenija[] {
				new IzlaznaStavkaMenija("Izlaz"),
				new FunkcionalnaStavkaMenija("Prikaz svih proizvoda") {

					@Override
					public void izvrsi() { ProizvodUI.prikazSvih(); }
					
				}, 
				new FunkcionalnaStavkaMenija("Prikaz svih porudzbina") {

					@Override
					public void izvrsi() { PorudzbinaUI.prikazSvih(); }
					
				}, 
				new FunkcionalnaStavkaMenija("Dodavanje porudzbine") {

					@Override
					public void izvrsi() { PorudzbinaUI.dodavanje(); }
					
				}, 
				new FunkcionalnaStavkaMenija("Izvestavanje") {

					@Override
					public void izvrsi() { IzvestavanjeUI.izvestavanje(); }
					
				}
			});

	}

}
