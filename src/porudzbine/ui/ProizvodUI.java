package porudzbine.ui;

import java.util.Collection;

import porudzbine.DAO.ProizvodDAO;
import porudzbine.model.Porudzbina;
import porudzbine.model.Proizvod;
import porudzbine.util.Konzola;

public class ProizvodUI {

	private static ProizvodDAO proizvodDAO;
	
	public static void setProizvodDAO(ProizvodDAO proizvodDAO) {
		ProizvodUI.proizvodDAO = proizvodDAO;
	}

	
	public static void prikazSvih() {
		try {
			Collection<Proizvod> proizvod = proizvodDAO.getAll();

			System.out.println();
			for (Proizvod itProizvod: proizvod) {
				System.out.println(itProizvod);
				System.out.println();
				for (Porudzbina itPorudzbina: itProizvod.getPorudzbinaSet()) {
					System.out.println(itPorudzbina);
				}
				System.out.println("\n\n");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("Došlo je do greške!");
		}
	}
	
	
	public static Proizvod pronalazenje() throws Exception {
		prikazSvih();

		String sifra = Konzola.ocitajString("Unesite sifru proizvoda: ");

		Proizvod proizvod = proizvodDAO.get(sifra);
		if (proizvod == null)
			Konzola.prikazi("Proizvod nije pronađeno!");

		return proizvod;
	}
}
