package porudzbine.model;

import java.time.LocalDateTime;
import java.util.Objects;

import porudzbine.util.Konzola;

public class Porudzbina {

	private final long id;
	private LocalDateTime datumIVreme;
	private String ulica;
	private int broj;
	private Proizvod Proizvod;
	
	
	public Porudzbina(long id, LocalDateTime datumIVreme, String ulica, int broj, Proizvod proizvod) {
		super();
		this.id = id;
		this.datumIVreme = datumIVreme;
		this.ulica = ulica;
		this.broj = broj;
		Proizvod = proizvod;
	}

	
	public boolean isDatumIVremeUOpsegu(LocalDateTime pocetak, LocalDateTime kraj) {
		return datumIVreme.compareTo(pocetak) >= 0 && datumIVreme.compareTo(kraj) <= 0;
	}
	
	
	@Override
	public String toString() {
		return "Porudzbine [id=" + id + ", datumIVreme=" + Konzola.formatiraj(datumIVreme) + ", ulica=" + ulica + ", broj=" + broj
				+ ", Proizvod=" + Proizvod.getNaziv() + "]";
	}


	public LocalDateTime getDatumIVreme() {
		return datumIVreme;
	}


	public void setDatumIVreme(LocalDateTime datumIVreme) {
		this.datumIVreme = datumIVreme;
	}


	public String getUlica() {
		return ulica;
	}


	public void setUlica(String ulica) {
		this.ulica = ulica;
	}


	public int getBroj() {
		return broj;
	}


	public void setBroj(int broj) {
		this.broj = broj;
	}


	public Proizvod getProizvod() {
		return Proizvod;
	}


	public void setProizvod(Proizvod proizvod) {
		Proizvod = proizvod;
	}


	public long getId() {
		return id;
	}


	@Override
	public int hashCode() {
		return Objects.hash(id);
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Porudzbina other = (Porudzbina) obj;
		return id == other.id;
	}
	
	

}
