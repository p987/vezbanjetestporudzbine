package porudzbine.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

public class Proizvod {

	private final long  id;
	private String sifra;
	private String naziv;
	private double cena;
	private boolean besplatnaDostava;
	private Set<Porudzbina> porudzbinaSet = new LinkedHashSet<>();
	
	
	public Proizvod(long id, String sifra, String naziv, double cena, boolean besplatnaDostava) {
		this.id = id;
		this.sifra = sifra;
		this.naziv = naziv;
		this.cena = cena;
		this.besplatnaDostava = besplatnaDostava;
	}
	
	
	public Collection<Porudzbina> getPoziviIzUliceUVremenskomOpsegu(String ulica, LocalDateTime pocetak, LocalDateTime kraj) {
		Collection<Porudzbina> porudzbineUOpsegu = new ArrayList<>();
		for (Porudzbina itPorudzbina: porudzbinaSet) {
			if (itPorudzbina.getUlica().equals(ulica) && itPorudzbina.isDatumIVremeUOpsegu(pocetak, kraj)) {
				porudzbineUOpsegu.add(itPorudzbina);
			}
		}
		return porudzbineUOpsegu;
	}

	@Override
	public String toString() {
		return "Proizvod [id=" + id + ", sifra=" + sifra + ", naziv=" + naziv + ", cena=" + cena
				+ ", besplatnaDostava=" + besplatnaDostava + "]";
	}


	public Set<Porudzbina> getPorudzbinaSet() {
		return Collections.unmodifiableSet(porudzbinaSet) ;
	}


	public void addPorudzbina(Porudzbina porudzbina) {
		this.porudzbinaSet.add(porudzbina);
	}
	
	
	public void addAllPorudzbinaSet(Set<Porudzbina> porudzbinaSet) {
		this.porudzbinaSet.addAll(porudzbinaSet);
	}


	public String getSifra() {
		return sifra;
	}


	public void setSifra(String sifra) {
		this.sifra = sifra;
	}


	public String getNaziv() {
		return naziv;
	}


	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}


	public double getCena() {
		return cena;
	}


	public void setCena(double cena) {
		this.cena = cena;
	}


	public boolean isBesplatnaDostava() {
		return besplatnaDostava;
	}


	public void setBesplatnaDostava(boolean besplatnaDostava) {
		this.besplatnaDostava = besplatnaDostava;
	}


	public long getId() {
		return id;
	}


	@Override
	public int hashCode() {
		return Objects.hash(id);
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Proizvod other = (Proizvod) obj;
		return id == other.id;
	}
}
