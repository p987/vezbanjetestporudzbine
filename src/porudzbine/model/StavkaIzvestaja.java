package porudzbine.model;

import java.time.LocalDateTime;

import porudzbine.util.Konzola;

public class StavkaIzvestaja {

	public final String sifra;
	public final String naziv;
	public final double ukupanPrihod;
	public final LocalDateTime poslednjiDatumIVreme;
	
	
	public static int compareBrojPoziva(StavkaIzvestaja stavka1, StavkaIzvestaja stavka2) {
		return -Double.compare(stavka1.ukupanPrihod, stavka2.ukupanPrihod);
	}


	public StavkaIzvestaja(String sifra, String naziv, double ukupanPrihod, LocalDateTime poslednjiDatumIVreme) {
		super();
		this.sifra = sifra;
		this.naziv = naziv;
		this.ukupanPrihod = ukupanPrihod;
		this.poslednjiDatumIVreme = poslednjiDatumIVreme;
	}


	@Override
	public String toString() {
		LocalDateTime nulto = LocalDateTime.parse("01.01.0001. 00:00", Konzola.getDateTimeFormatter());
		return "StavkaIzvestaja [sifra=" + sifra + ", naziv=" + naziv + 
				", poslednjiDatumIVreme=" + ((poslednjiDatumIVreme.compareTo(nulto) != 0)?(Konzola.formatiraj(poslednjiDatumIVreme)) : ("nepoznato")) + 
				", ukupanPrihod=" + ukupanPrihod + "]";
	}
	
	

}
