package porudzbine.DAOImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import porudzbine.DAO.ProizvodDAO;
import porudzbine.model.Porudzbina;
import porudzbine.model.Proizvod;

public class ProizvodDAOImpl implements ProizvodDAO {

	private Connection conn;
	
	public ProizvodDAOImpl(Connection conn) {
		this.conn = conn;
	}

	@Override
	public Collection<Proizvod> getAll() throws Exception {
	Map<Long, Proizvod> proizvodMap = new LinkedHashMap<>();
		
		String sql = 
				"SELECT pr.id AS ProizvodId, pr.sifra, pr.naziv, pr.cena, pr.besplatnaDostava, \r\n"
				+ "po.id AS PorudzbinaId, po.datumIVreme, po.ulica, po.broj FROM proizvod pr\r\n"
				+ "JOIN porudzbine po ON pr.id = po.proizvodId;";
		try (PreparedStatement stmt = conn.prepareStatement(sql)) {
			try (ResultSet rset = stmt.executeQuery()) {
				while (rset.next()) {
					int kolona = 0;
					long prId = rset.getLong(++kolona);
					String prSifra = rset.getString(++kolona);
					String prNaziv = rset.getString(++kolona);
					double prCena = rset.getDouble(++kolona);
					boolean prBesplatnaDostava = rset.getBoolean(++kolona);
					
					Proizvod proizvod = proizvodMap.get(prId);
					if (proizvod == null) {
						proizvod = new Proizvod(prId, prSifra, prNaziv, prCena, prBesplatnaDostava);
						proizvodMap.put(proizvod.getId(), proizvod);
					}
					long poId = rset.getLong(++kolona);
					if (poId != 0) {
						LocalDateTime poDatumIVreme = rset.getTimestamp(++kolona).toLocalDateTime();
						String poUlica = rset.getString(++kolona);
						int poBroj = rset.getInt(++kolona);

						Porudzbina porudzbina = new Porudzbina(poId, poDatumIVreme, poUlica, poBroj, proizvod);
						proizvod.addPorudzbina(porudzbina);
					}

				}
			}
		}
		return proizvodMap.values();
	}


	@Override
	public Proizvod get(String sifra) throws Exception {
		Proizvod proizvod = null;
		
		String sql = "SELECT pr.id AS ProizvodId, pr.sifra, pr.naziv, pr.cena, pr.besplatnaDostava,\r\n"
				+ "po.id AS PorudzbinaId, po.datumIVreme, po.ulica, po.broj FROM proizvod pr\r\n"
				+ "JOIN porudzbine po ON pr.id = po.proizvodId WHERE pr.sifra = ?;";
		try (PreparedStatement stmt = conn.prepareStatement(sql)) {
			stmt.setString(1, sifra);
			try (ResultSet rset = stmt.executeQuery()) {
				while (rset.next()) {
					int kolona = 0;
					long prId = rset.getLong(++kolona);
					String prSifra = rset.getString(++kolona);
					String prNaziv = rset.getString(++kolona);
					double prCena = rset.getDouble(++kolona);
					boolean prBesplatnaDostava = rset.getBoolean(++kolona);
					
					if (proizvod == null) {
						proizvod = new Proizvod(prId, prSifra, prNaziv, prCena, prBesplatnaDostava);
					}
					
					long poId = rset.getLong(++kolona);
					if (poId != 0) {
						LocalDateTime poDatumIVreme = rset.getTimestamp(++kolona).toLocalDateTime();
						String poUlica = rset.getString(++kolona);
						int poBroj = rset.getInt(++kolona);

						Porudzbina porudzbina = new Porudzbina(poId, poDatumIVreme, poUlica, poBroj, proizvod);
						proizvod.addPorudzbina(porudzbina);
					}
				}
			}
		}
		return proizvod;
	}
}