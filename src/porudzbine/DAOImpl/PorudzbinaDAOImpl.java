package porudzbine.DAOImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import porudzbine.DAO.PorudzbinaDAO;
import porudzbine.model.Porudzbina;
import porudzbine.model.Proizvod;

public class PorudzbinaDAOImpl implements PorudzbinaDAO {

	private Connection conn;
	
	public PorudzbinaDAOImpl(Connection conn) {
		this.conn = conn;
	}

	@Override
	public Collection<Porudzbina> getAll() throws Exception {
Map<Long, Porudzbina> porudzbinaMap = new LinkedHashMap<>();
		
		String sql = "SELECT po.id AS PorudzbinaId, po.datumIVreme, po.ulica, po.broj, \r\n"
				+ "pr.id AS ProizvodId, pr.sifra, pr.naziv, pr.cena, pr.besplatnaDostava \r\n"
				+ "FROM porudzbine po\r\n"
				+ "JOIN proizvod pr ON po.proizvodId = pr.id;";
		try (PreparedStatement stmt = conn.prepareStatement(sql)) {
			try (ResultSet rset = stmt.executeQuery()) {
				while (rset.next()) {
					int kolona = 0;
					long poId = rset.getLong(++kolona);
					LocalDateTime poDatumIVreme = rset.getTimestamp(++kolona).toLocalDateTime();
					String poUlica = rset.getString(++kolona);
					int poBroj = rset.getInt(++kolona);
								
					Porudzbina porudzbina = porudzbinaMap.get(poId);
					if (porudzbina == null) {
						porudzbina = new Porudzbina(poId, poDatumIVreme, poUlica, poBroj, null) ;
						porudzbinaMap.put(porudzbina.getId(), porudzbina);
					}
					
					long prId = rset.getLong(++kolona);
					if (prId != 0) {
						String prSifra = rset.getString(++kolona);
						String prNaziv = rset.getString(++kolona);
						double prCena = rset.getDouble(++kolona);
						boolean prBesplatnaDostava = rset.getBoolean(++kolona);

						Proizvod proizvod = new Proizvod(prId, prSifra, prNaziv, prCena, prBesplatnaDostava);
						porudzbina.setProizvod(proizvod);;
					}

				}
			}
		}
		return porudzbinaMap.values();
	}

	@Override
	public void add(Porudzbina porudzbina) throws Exception {
		String sql = "INSERT INTO porudzbine (datumIVreme, ulica, broj, proizvodId) VALUES (?, ?, ?, ?);";
		try (PreparedStatement stmt = conn.prepareStatement(sql)) {
			int param = 0;
			stmt.setTimestamp(++param, Timestamp.valueOf(porudzbina.getDatumIVreme()));
			stmt.setString(++param, porudzbina.getUlica());
			stmt.setInt(++param, porudzbina.getBroj());
			stmt.setLong(++param, porudzbina.getProizvod().getId());

			stmt.executeUpdate();
		}
	}
}
