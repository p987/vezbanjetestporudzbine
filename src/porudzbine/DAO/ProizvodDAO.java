package porudzbine.DAO;

import java.util.Collection;

import porudzbine.model.Proizvod;

public interface ProizvodDAO {
	public Collection<Proizvod> getAll() throws Exception;
	public Proizvod get(String sifra) throws Exception;
}
