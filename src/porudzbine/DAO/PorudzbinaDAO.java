package porudzbine.DAO;

import java.util.Collection;

import porudzbine.model.Porudzbina;

public interface PorudzbinaDAO {
	public Collection<Porudzbina> getAll() throws Exception;
	public void add(Porudzbina porudzbina) throws Exception;
}
